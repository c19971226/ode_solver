#include "solve_ivp_each_step.h"

void
solve_ivp_each_step(
    odefunc f, int dim, double h, double t, const double * y, double * y_new)
{
  double phi[dim];

  f(t, y, phi);
  for (int i = 0; i < dim; i++)
    y_new[i] = y[i] + phi[i] * h;

  return;
}

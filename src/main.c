#include <stdio.h>
#include <math.h>

#include "solve_ivp.h"

static int
func(double t, const double * y, double * ydot)
{
  ydot[0] = (1 + 4 * t) * sqrt(y[0]);
  return 0;
}

static void
log_func(double t, double * y)
{
  printf("%12.7f, %12.7f\n", t, y[0]);
  return;
}

int
main()
{
  ODE_Conf conf = {
    .t_start = 0.,
    .t_end = 1.,
    .y_init = (double[]) { 1. },
    .step_size = 0.25,
    .dim = 1,
    .callback = &log_func};


  printf("%12s, %12s\n", "t", "y");
  solve_ivp(&func, &conf);

  return 0;
}

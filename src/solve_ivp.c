#include "solve_ivp.h"
#include "solve_ivp_each_step.h"

void
solve_ivp(odefunc f, ODE_Conf * conf)
{
  int dim = conf->dim;
  double y[dim], y_new[dim];
  double t_end = conf->t_end;
  double h = conf->step_size;

  for (int i = 0; i < dim; i++)
    y[i] = conf->y_init[i];

  for (double t = conf->t_start; t <= t_end; t += h)
  {
    solve_ivp_each_step(f, conf->dim, conf->step_size,
        t, y, y_new);
    conf->callback(t, y);
    for (int i = 0; i < dim; i++)
      y[i] = y_new[i];
  }

  return;
}

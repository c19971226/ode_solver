#include "solve_ivp_each_step.h"

void
solve_ivp_each_step(
    odefunc f, int dim, double h, double t, const double * y, double * y_new)
{
  double k1[dim], k2[dim], y2[dim];

  f(t, y, k1);
  for (int i = 0; i < dim; i++)
    y2[i] = y[i] + 0.5 * k1[i] * h;
  f(t + 0.5 * h, y2, k2);

  for (int i = 0; i < dim; i++)
    y_new[i] = y[i] + k2[i] * h;

  return;
}


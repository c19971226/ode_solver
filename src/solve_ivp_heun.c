#include "solve_ivp_each_step.h"

void
solve_ivp_each_step(
    odefunc f, int dim, double h, double t, const double * y, double * y_new)
{
  double k1[dim], k2[dim], y2[dim];

  f(t, y, k1);
  for (int i = 0; i < dim; i++)
    y2[i] = y[i] + k1[i] * h;
  f(t + h, y2, k2);

  for (int i = 0; i < dim; i++)
    y_new[i] = y[i] + (k1[i] + k2[i]) * h / 2.;

  return;
}


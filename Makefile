# Reference: https://makefiletutorial.com/#makefile-cookbook

TARGET    := main

# Directories
BIN_DIRS  := bin
OBJ_DIRS  := obj
INC_DIRS  := inc
SRC_DIRS  := src
DIRS      := $(BIN_DIRS) $(OBJ_DIRS) $(INC_DIRS) $(SRC_DIRS)

# Files
SRCS			:= $(wildcard $(SRC_DIRS)/*.c)
OBJS 			:= $(SRCS:$(SRC_DIRS)/%.c=$(OBJ_DIRS)/%.o)
DEPS      := $(OBJS:%.o=%.d)
BINS      := $(addprefix $(BIN_DIRS)/, $(TARGET))

# Compiler command
CC 				:= gcc
CFLAGS    += -Wall -O2 -g
INC_FLAGS += $(addprefix -I, $(INC_DIRS))
CPPFLAGS  += -MMD -MP $(INC_FLAGS)
LDFLAGS  	+= -lm

# Debugger command
DB        := gdb

$(BINS): $(OBJS) $(BIN_DIRS)
	$(CC) $(LDFLAGS) $(OBJS) -o $@

$(OBJ_DIRS)/%.o: $(SRC_DIRS)/%.c $(OBJ_DIRS)
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $< -o $@

$(DIRS):
	mkdir $@

.PHONY: all clean run tags log

all: $(BINS)

clean:
	rm -f $(OBJ_DIRS)/*
	rm -f $(BIN_DIRS)/*

run: $(BINS)
	$<

log: $(BINS)
	$< > result.csv

tags: $(SRCS)
	ctags -R .

debug: $(BINS)
	$(DB) $<

-include $(DEPS)

#ifndef _POLYNOMIAL_H_
#define _POLYNOMIAL_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Compute the value of polynomial
 * * @coef coefficients of the polynomial in ascent order
 * * @size array size of @coef
 * * @x    value of independent variable
 */
double polynomial(double *coef, int size, double x);

#ifdef __cplusplus
}
#endif
#endif

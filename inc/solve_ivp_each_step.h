#ifndef _SOLVE_IVP_EACH_STEP_H_
#define _SOLVE_IVP_EACH_STEP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "ode.h"

void
solve_ivp_each_step(
    odefunc f, int dim, double h, double t, const double * y, double * y_new);

#ifdef __cplusplus
}
#endif
#endif

#ifndef _ODE_H_
#define _ODE_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Ordinary differential equation in explicit form - $\dot y = f(t, y)$
 * * @t    The independent variable
 * * @y    The state vector
 * * @ydot Return value of $f$
 */
typedef int (* odefunc)(double t, const double * y, double * ydot);

typedef struct _ode_conf {
  double t_start;
  double t_end;
  double * y_init;
  double step_size;
  int dim;
  void (* callback)(double, double *);
} ODE_Conf;

#ifdef __cplusplus
}
#endif
#endif

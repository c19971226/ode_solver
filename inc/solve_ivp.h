#ifndef _SOLVE_IVP_H_
#define _SOLVE_IVP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "ode.h"

void
solve_ivp(odefunc f, ODE_Conf * conf);


#ifdef __cplusplus
}
#endif
#endif

#ifndef _RK_H_
#define _RK_H_

#ifdef __cplusplus
extern "C" {
#endif


void
odefunc(double t, const double * y, double * dydx);

void
rk_iterate(
    double (*f)(double, const double *, double *),
    double t, const double * y, double * dydx);

void
solve_ivp_rk(
    double (*f)(double, const double *, double *),
    RK_Conf * conf)
{
  double t_end = conf->t_end;
  double h = conf->step_size;
  double num_of_iter = (t_start - t_end) / h;

  double t = conf->t_start;
  double y = conf->y_init;

  for (int i = 0; i < num_of_iter; i++)
  {
    rk_iterate(f,
  }

}

#ifdef __cplusplus
}
#endif
#endif

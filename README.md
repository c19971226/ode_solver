# ODE Solver

This is a simple, explicit Runge-Kutta ODE solver library made by C.
Implemented methods are as follows:

* Euler's method (1st order)
* Heun's method (2nd order)
* The midpoint method (2nd order)
* Ralston's method (2nd order)
* 3rd order Runge-Kutta method
* 4th order Runge-Kutta method

## Usage

See `main.c` file to understand how to set up the ODE problem and run the solver.